use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    core::{math::Vector3, transform::Transform},
    ecs::prelude::{Component, DenseVecStorage},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
};

pub struct Frogger;

pub const ARENA_HEIGHT: f32 = 130.0;
pub const ARENA_WIDTH: f32 = 130.0;
pub const FROG_WIDTH: f32 = 10.0;
pub const FROG_HEIGHT: f32 = 10.0;

#[derive(PartialEq, Eq)]
pub enum Side {
    Left,
    Right,
}

pub struct Frog {
    pub side: Side,
    pub width: f32,
    pub height: f32,
}

impl Frog {
    fn new(side: Side) -> Frog {
        Frog {
            side,
            width: FROG_WIDTH,
            height: FROG_HEIGHT,
        }
    }
}

impl Component for Frog {
    type Storage = DenseVecStorage<Self>;
}

impl SimpleState for Frogger {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        let sprite_sheet_handle = load_sprite_sheet(world);

        world.register::<Frog>();

        initialise_frogs(world, sprite_sheet_handle);
        initialise_camera(world);
    }
}

fn initialise_camera(world: &mut World) {
    // Setup camera in a way that our screen covers whole arena and (0, 0) is in the bottom left.
    let mut transform = Transform::default();
    transform.set_translation_xyz(ARENA_WIDTH * 0.5, ARENA_HEIGHT * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(ARENA_WIDTH, ARENA_HEIGHT))
        .with(transform)
        .build();
}

fn initialise_frogs(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    let mut left_transform = Transform::default();
    let mut right_transform = Transform::default();

    let y = FROG_HEIGHT; // TODO
    left_transform.set_translation_xyz(ARENA_WIDTH * 0.25, y, 0.0);
    left_transform.set_scale(Vector3::new(0.15, 0.15, 0.15));

    right_transform.set_translation_xyz(ARENA_WIDTH * 0.75, y, 0.0);
    right_transform.set_scale(Vector3::new(0.15, 0.15, 0.15));

    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet.clone(),
        sprite_number: 0,
    };

    world
        .create_entity()
        .with(sprite_render.clone())
        .with(Frog::new(Side::Left))
        .with(left_transform)
        .build();

    world
        .create_entity()
        .with(sprite_render.clone())
        .with(Frog::new(Side::Right))
        .with(right_transform)
        .build();
}

fn load_sprite_sheet(world: &mut World) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/frogger_spritesheet.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "texture/frogger_spritesheet.ron",
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}
