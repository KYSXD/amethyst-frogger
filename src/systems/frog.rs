use amethyst::core::Transform;
use amethyst::ecs::{Join, Read, ReadStorage, Resources, System, WriteStorage};
use amethyst::input::InputEvent;

// You'll have to mark FROG_HEIGHT as public in pong.rs
use crate::frogger::{Frog, Side, FROG_HEIGHT, FROG_WIDTH};

use amethyst::core::shrev::{EventChannel, ReaderId};

use amethyst::ecs::prelude::SystemData;

pub struct FrogSystem {
    event_reader: Option<ReaderId<InputEvent<String>>>,
}

impl FrogSystem {
    pub fn new() -> Self {
        FrogSystem { event_reader: None }
    }
}

impl<'s> System<'s> for FrogSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Frog>,
        Read<'s, EventChannel<InputEvent<String>>>,
    );

    fn run(&mut self, (mut transforms, frogs, events): Self::SystemData) {
        let mut left_h_mov = vec![];
        let mut left_v_mov = vec![];
        let mut right_h_mov = vec![];
        let mut right_v_mov = vec![];

        for event in events.read(&mut self.event_reader.as_mut().unwrap()) {
            match event {
                InputEvent::ActionPressed(action) => match action.as_ref() {
                    "LeftFrog.up" => {
                        left_v_mov.push(FROG_HEIGHT);
                    }
                    "LeftFrog.down" => {
                        left_v_mov.push(-FROG_HEIGHT);
                    }
                    "LeftFrog.left" => {
                        left_h_mov.push(-FROG_WIDTH / 2.0);
                    }
                    "LeftFrog.right" => {
                        left_h_mov.push(FROG_WIDTH / 2.0);
                    }
                    "RightFrog.up" => {
                        right_v_mov.push(FROG_HEIGHT);
                    }
                    "RightFrog.down" => {
                        right_v_mov.push(-FROG_HEIGHT);
                    }
                    "RightFrog.left" => {
                        right_h_mov.push(-FROG_WIDTH / 2.0);
                    }
                    "RightFrog.right" => {
                        right_h_mov.push(FROG_WIDTH / 2.0);
                    }
                    _ => {
                        println!("Received an event: {:?}", event);
                    }
                },
                _ => (),
            }
        }

        for (frog, transform) in (&frogs, &mut transforms).join() {
            match frog.side {
                Side::Left => {
                    match left_h_mov.iter().last() {
                        Some(mov) => {
                            transform.prepend_translation_x(mov.clone());
                        }
                        _ => (),
                    }
                    match left_v_mov.iter().last() {
                        Some(mov) => {
                            transform.prepend_translation_y(mov.clone());
                        }
                        _ => (),
                    }
                }
                Side::Right => {
                    match right_h_mov.iter().last() {
                        Some(mov) => {
                            transform.prepend_translation_x(mov.clone());
                        }
                        _ => (),
                    }
                    match right_v_mov.iter().last() {
                        Some(mov) => {
                            transform.prepend_translation_y(mov.clone());
                        }
                        _ => (),
                    }
                }
            }
        }
    }

    fn setup(&mut self, res: &mut Resources) {
        Self::SystemData::setup(res);

        self.event_reader = Some(
            res.fetch_mut::<EventChannel<InputEvent<String>>>()
                .register_reader(),
        );
    }
}
